package com.example.diegoanchirayco.app_myhome;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class RecibosPendientesFragment extends Fragment implements View.OnClickListener{

    RecyclerView _rviRecibosPendientes;
    RecibosPendientesItemsActivity _entItem;
    OnRescibosPendientesFragmentListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recibos_pendientes, container, false);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        _rviRecibosPendientes = view.findViewById(R.id.rviRecibosPendientes);
        _rviRecibosPendientes.setLayoutManager(new LinearLayoutManager(getActivity()));

        List<Integer> _lista = new ArrayList<>();
        for (int i = 1; i <= 5; i++)
        {
            _lista.add(i);
        }

        _entItem = new RecibosPendientesItemsActivity(_lista);
        _rviRecibosPendientes.setAdapter(_entItem);
        /**/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Utilizado para instanciar interface
        try {
            listener = (OnRescibosPendientesFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onClick(View view) {
        listener.onBotonSeleccionado("Boton fragment seleccionado!");
    }

    public interface OnRescibosPendientesFragmentListener {
        public void onBotonSeleccionado(String mensaje);
    }
}
