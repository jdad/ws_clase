package com.example.diegoanchirayco.app_myhome;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

public class RecibosPendientesItemsActivity extends RecyclerView.Adapter<RecibosPendientesItemsActivity.ViewHolder> {

    List<Integer> _listItems;

    public RecibosPendientesItemsActivity(List<Integer> list) {

        this._listItems = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_recibos_pendientes_items, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int _valor = _listItems.get(position);
        String _dato = GetNamePeriodo(_valor);
        holder.tviItem.setText("• Recibo  " + _dato);
    }

    private String GetNamePeriodo(int i)
    {
        String MES[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        Calendar c1 = Calendar.getInstance();
        int annio = c1.get(Calendar.YEAR);
        String mes = MES[i];

        return mes + " " + annio;
    }

    @Override
    public int getItemCount() {
        return _listItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tviItem;
        ImageButton ibuItem;

        public ViewHolder(View v) {
            super(v);

            tviItem = v.findViewById(R.id.tviReciboItem);
            //ibuItem = v.findViewById(R.id.ibuVerReciboItem);
        }
    }
}
