package com.example.diegoanchirayco.app_myhome;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class RecibosHistoricosFragment extends Fragment {

    RecyclerView _rviRecibosHistoricos;
    RecibosHistoricosItemsActivity _entItem;
    //OnRescibosPendientesFragmentListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recibos_historicos, container, false);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        _rviRecibosHistoricos = view.findViewById(R.id.rviRecibosHistoricos);
        _rviRecibosHistoricos.setLayoutManager(new LinearLayoutManager(getActivity()));

        List<Integer> _lista = new ArrayList<>();
        for (int i = 1; i <= 10; i++)
        {
            _lista.add(i);
        }

        _entItem = new RecibosHistoricosItemsActivity(_lista);
        _rviRecibosHistoricos.setAdapter(_entItem);
        /**/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Utilizado para instanciar interface
        try {
            //listener = (OnRescibosPendientesFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
