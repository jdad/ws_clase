package com.example.diegoanchirayco.app_myhome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button _butSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _butSesion = findViewById(R.id.butSesion);
        _butSesion.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.butSesion :{
                Intent _intent = new Intent(this, PanelActivity.class);
                startActivity(_intent);
            }
            break;
        }
    }
}
