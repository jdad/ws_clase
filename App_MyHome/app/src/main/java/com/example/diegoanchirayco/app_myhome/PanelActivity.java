package com.example.diegoanchirayco.app_myhome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toolbar;

public class PanelActivity extends AppCompatActivity implements View.OnClickListener{

    Button _butRecibos;
    Button _butNoticias;
    Button _butMensaje;
    Button _butTienda;
    Button _butInfomes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);

        _butRecibos = findViewById(R.id.butRecibos);
        _butNoticias = findViewById(R.id.butNoticias);
        _butMensaje = findViewById(R.id.butMensajes);
        _butTienda = findViewById(R.id.butTienda);
        _butInfomes = findViewById(R.id.butInformes);

        _butRecibos.setOnClickListener(this);
        //_butNoticias.setOnClickListener(this);
        //_butMensaje.setOnClickListener(this);
        //_butTienda.setOnClickListener(this);
        //_butInfomes.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.butRecibos :{
                Intent _intent = new Intent(this, RecibosActivity.class);
                startActivity(_intent);
            }
            break;
            case R.id.butNoticias :{
                Intent _intent = new Intent(this, null);
                startActivity(_intent);
            }
            break;
            case R.id.butMensajes :{
                Intent _intent = new Intent(this, null);
                startActivity(_intent);
            }
            break;
            case R.id.butTienda :{
                Intent _intent = new Intent(this, null);
                startActivity(_intent);
            }
            break;
            case R.id.butInformes :{
                Intent _intent = new Intent(this, null);
                startActivity(_intent);
            }
            break;
        }
    }
}
