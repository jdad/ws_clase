package com.example.diegoanchirayco.app_myhome;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;

public class RecibosActivity extends AppCompatActivity implements View.OnClickListener, RecibosPendientesFragment.OnRescibosPendientesFragmentListener{

    RadioButton _rbuRecibosPendientes;
    RadioButton _rbuRecibosHistoricos;
    FrameLayout _fmeContent;

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibos);

        _rbuRecibosPendientes = findViewById(R.id.rbuRecibosPendientes);
        _rbuRecibosHistoricos = findViewById(R.id.rbuRecibosHistoricos);
        _fmeContent =  findViewById(R.id.fmeContent);

        _rbuRecibosPendientes.setOnClickListener(this);
        _rbuRecibosHistoricos.setOnClickListener(this);

        loadFragment(new RecibosPendientesFragment());
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.rbuRecibosPendientes :{
                loadFragment(new RecibosPendientesFragment());
            }
            break;
            case R.id.rbuRecibosHistoricos :{
                loadFragment(new RecibosHistoricosFragment());
            }
            break;
        }
    }

    private void loadFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.fmeContent, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onBotonSeleccionado(String mensaje) {

    }
}
